'use strict'

/** @type {import('@adonisjs/framework/src/Route/Manager'} */

const Route = use('Route')

Route.group( () => {

    Route.post( 'login',                        'AuthController.login'          );
    Route.post( 'registro',                     'AuthController.registro'       );
    Route.put(  'perfil',                       'AuthController.perfil'         ).middleware( ['auth:jwt'] );

    Route.get(  'cinema',                       'CinemaController.index'        );
    Route.get(  'cinema/:id',                   'CinemaController.buscarCinema' );

    Route.get(  'genero',                       'GeneroController.index'        );

    Route.get(  'pelicula/:cinemaId/cinema',      'PeliculaController.cinema'     );
    Route.get(  'pelicula/:peliculaId/pelicula',  'PeliculaController.pelicula'   );

    Route.post( 'reserva',                      'ReservaController.save'          ).middleware( ['auth:jwt'] );
    Route.get(  'reserva/ultima',               'ReservaController.ultimaReserva' ).middleware( ['auth:jwt'] );
    Route.get(  'reserva',                      'ReservaController.index'         ).middleware( ['auth:jwt'] );

} ).prefix( 'api/v1' );


Route.on('/').render('welcome')
// Route.post('data', 'AuthController.data');