'use strict'

const Model = use('Model')

class Cinema extends Model {
    
    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    presentacion_peliculas() {
        return this.hasMany( 'App/Models/PresentacionPelicula' );
    }

    salas() {
        return this.hasMany( 'App/Models/Sala' );
    }

}

module.exports = Cinema
