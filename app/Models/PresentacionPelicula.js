'use strict'

const Model = use('Model')

class PresentacionPelicula extends Model {

    pelicula() {
        return this.belongsTo( 'App/Models/Pelicula' );
    }

    cinema() {
        return this.belongsTo( 'App/Models/Cinema' );
    }

    sala() {
        return this.belongsTo( 'App/Models/Sala' );
    }

    presentacion_pelicula_horarios() {
        return this.hasMany( 'App/Models/PresentacionPeliculaHorario' );
    }

}

module.exports = PresentacionPelicula
