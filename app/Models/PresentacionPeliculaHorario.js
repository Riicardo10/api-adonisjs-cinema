'use strict'

const Model = use('Model')

class PresentacionPeliculaHorario extends Model {

    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    presentacion_pelicula() {
        return this.belongsTo(  'App/Model/PresentacionPelicula' );
    }

    salas() {
        return this.hasMany(    'App/Models/Sala' );
    }

    reservas() {
        return this.hasMany(    'App/Models/Reserva' );
    }

}

module.exports = PresentacionPeliculaHorario
