'use strict'

const Model = use('Model')

class Genero extends Model {

    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    peliculas() {
        return this.belongsToMany( 'App/Models/Pelicula' );
    }

}

module.exports = Genero
