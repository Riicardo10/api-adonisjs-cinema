'use strict'

const Model = use('Model')

class Sala extends Model {
    
    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    /*cinemas() {
        return this.belongsTo( 'App/Models/Cinema' );
    }*/

    presentacion_pelicula() {
        return this.hasMany( 'App/Models/PresentacionPelicula' );
    }

}

module.exports = Sala
