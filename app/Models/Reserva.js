'use strict'

const Model = use('Model')

class Reserva extends Model {

    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    asientos() {
        return this.hasMany( 'App/Models/Asiento' );
    }

    presentacion_pelicula_horario() {
        return this.belongsTo( 'App/Models/PresentacionPeliculaHorario' );
    }

}

module.exports = Reserva
