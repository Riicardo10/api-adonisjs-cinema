'use strict'

const Model = use('Model')

class Pelicula extends Model {
    
    static get createdAtColumn() {
        return null;
    }

    static get updatedAtColumn() {
        return null;
    }

    generos() {
        return this.belongsToMany( 'App/Models/Genero' );
    }

}

module.exports = Pelicula
