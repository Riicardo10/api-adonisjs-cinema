'use strict'

const Model = use('Model')

class Cliente extends Model {

    /*user() {
        return this.belongsTo( 'App/Models/User' );
    }*/

    reservas() {
        return this.hasMany( 'App/Models/Reserva' );
    }

}

module.exports = Cliente
