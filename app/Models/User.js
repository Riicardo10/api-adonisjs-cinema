  'use strict'

const Hash = use('Hash')
const Model = use('Model')
const Cliente = use('App/Models/Cliente')

class User extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userInstance) => {
            if (userInstance.password) {
                userInstance.password = await Hash.make(userInstance.password)
            }
        })
        this.addHook('afterCreate', async (userInstance) => {
            let cliente = new Cliente();
            userInstance.cliente().save(cliente);
          })
    }

    tokens () {
        return this.hasMany( 'App/Models/Token')
    }

    cliente() {
        return this.hasOne( 'App/Models/Cliente' );
    }

}

module.exports = User
