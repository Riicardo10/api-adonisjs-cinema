'use strict'

const Genero = use( 'App/Models/Genero' );

class GeneroController {

    async index( {request, response, auth} ) {
        const generos = await Genero.all();
        return ( {data: generos} );
    }

}

module.exports = GeneroController
