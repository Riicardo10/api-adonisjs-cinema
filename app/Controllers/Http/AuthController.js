'use strict'

const User = use( 'App/Models/User' );

class AuthController {

    async login( {request, response, auth} ) {
        const   {data}  = request.all();
        const   logged  = await auth.attempt( data.email, data.password, true );
        return  response.json( logged );
    };

    async registro( {request, response} ) {
        const { data } = request.all();
        const usuario = new User();
        usuario.username = data.username;
        usuario.email = data.email;
        usuario.password = data.password;
        await usuario.save();
        return response.json(usuario);
    };

    async perfil( {request, response, auth} ) {
        let     user        = await auth.getUser();
        const   user_input  = request.input( 'user' );
        user.email          = user_input( 'email' );
        user.username       = user_input( 'username' );
        await user.save();
        const   logged      = await auth.generate( user, true );
        return  response.json( logged );
    };


}

module.exports = AuthController
