'use strict'

const Cinema = use( 'App/Models/Cinema' );
const moment = require( 'moment' );

class CinemaController {

    async index( {request, response, auth} ) {
        const cinemas = await Cinema.query().withCount( 'salas' ).fetch();
        return response.json( {data: cinemas} );
    }

    async buscarCinema( {request, response, params, auth} ) {
        const cinema = await Cinema.find( params.id );
        await cinema.loadMany( {
            presentacion_peliculas: (presentacion_peliculas) => {
                presentacion_peliculas
                    .select('id', 'pelicula_id', 'sala_id' )
                    // .where( 'fecha', moment( new Date() ).format( 'YYYY-MM-DD' ) )
                    .where( 'fecha', moment( new Date() ).format( '2018-09-28' ) )
                    .with(  'presentacion_pelicula_horarios', (presentacion_pelicula_horarios) => {
                        presentacion_pelicula_horarios.where( 'horario', '>=', new Date().getHours() )
                            .with( 'reservas', (reservas) => {
                                reservas.with( 'asientos' );
                            } )
                    } )
                    .with( 'pelicula', (pelicula) => {
                        pelicula.with( 'generos', (generos) => {
                            generos.select( 'nombre' );
                        } )
                    } )
                    .with( 'sala' )

            }
        } );
        return response.json( {data: cinema} );
    }
    
}

module.exports = CinemaController
