'use strict'

// const Pelicula =             use(       'App/Models/Pelicula' );
const Cinema =               use(       'App/Models/Cinema' );
const PresentacionPelicula = use(       'App/Models/PresentacionPelicula' );
const moment =               require(   'moment' );

class PeliculaController {

    async cinema( {request, response, params} ) {
        const cinema = await Cinema.find( params.cinemaId );
        await cinema.loadMany( {
            presentacion_peliculas: (presentacion_peliculas) => {
                presentacion_peliculas
                    .select(    'id', 'pelicula_id', 'sala_id' )
                    .where(     'fecha', moment( new Date() ).format( 'YYYY-MM-DD' ) )
                    .with(      'presentacion_pelicula_horarios', (presentacion_pelicula_horario) => {
                        presentacion_pelicula_horario.where( 'horario', '>=', new Date().getHours() )
                    } )
                    .with(      'pelicula', (pelicula) => {
                        pelicula.with( 'generos', (generos) => {
                            generos.select( 'nombre' );
                        } )
                    } )
                    .with( 'sala' )
            }
        } );
        return response.json( {data: cinema} ); 
    }

    async pelicula ( { request, response, params } ) {
        const pelicula = await PresentacionPelicula.findBy( 'pelicula_id', params.pelicula );
        await pelicula.loadMany( {
            presentacion_pelicula_horarios: (presentacion_pelicula_horarios) => {
                presentacion_pelicula_horarios
                    .where( 'horario', '>=', new Date().getHours() )
                    .with(  'reservas', (reservas) => {
                        reservas
                            .with( 'asientos' )
                    } )
            },
            pelicula: (pelicula) => {
                pelicula
                    .with( 'generos', (generos) => {
                        generos
                            .select( 'nombre' )
                    } )
            },
            sala: null
        } );
        return response.json( {data: pelicula} );
    }


}

module.exports = PeliculaController
