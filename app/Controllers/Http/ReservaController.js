'use strict'

const Reserva =     use( 'App/Models/Reserva' );
const Asiento =     use( 'App/Models/Asiento' );
const Cliente =     use( 'App/Models/Cliente' );
const Database =    use( 'Database' );

class ReservaController {

    async index( {request, response, auth} ) {
        const user      = await auth.getUser();
        const cliente   = await Cliente.findBy( 'user_id',      user.id );
        await cliente.loadMany( {
            reservas: (reservas) => {
                reservas
                    .where(     'cliente_id', cliente.id ) // ????
                    .orderBy(   'id', 'desc' )
                    .with(      'asientos' )
                    .with(      'presentacion_pelicula_horario', async presentacion_pelicula_horario => {
                        presentacion_pelicula_horario
                            .select(    'id', 'presentacion_pelicula_id' )
                            .with(      'presentacion_pelicula', (presentacion_pelicula) => {
                                presentacion_pelicula
                                    .select(    'id', 'cinema_id', 'pelicula_id', 'sala_id' )
                                    .with(      'pelicula', (pelicula) => {
                                        pelicula
                                            .select(    'id', 'nombre', 'director' )
                                            .with(      'generos', (generos) => {
                                                generos
                                                    .select( 'id', 'nombre' )
                                            } )
                                    } )
                                    .with(      'cinema', (cinema) => {
                                        cinema
                                            .select( 'id', 'nombre', 'direccion', 'telefono', 'asientos' )
                                    } )
                            } )
                    } )
            }
        } );

        return response.json( {data: cliente} );
    }

    async ultimaReserva( {request, response, auth} ) {
        const user      = await auth.getUser();
        const cliente   = await Cliente.findBy( 'user_id',      user.id );
        await cliente.loadMany( {
            reservas: (reservas) => {
                reservas
                    .where(     'cliente_id', cliente.id ) // ????
                    .limit(     1 )
                    .orderBy(   'id', 'desc' )
                    .with(      'asientos' )
                    .with(      'presentacion_pelicula_horario', async presentacion_pelicula_horario => {
                        presentacion_pelicula_horario
                            .select(    'id', 'presentacion_pelicula_id' )
                            .with(      'presentacion_pelicula', (presentacion_pelicula) => {
                                presentacion_pelicula
                                    .select(    'id', 'cinema_id', 'pelicula_id', 'sala_id' )
                                    .with(      'pelicula', (pelicula) => {
                                        pelicula
                                            .select(    'id', 'nombre', 'director' )
                                            .with(      'generos', (generos) => {
                                                generos
                                                    .select( 'id', 'nombre' )
                                            } )
                                    } )
                                    .with(      'cinema', (cinema) => {
                                        cinema
                                            .select( 'id', 'nombre', 'direccion', 'telefono', 'asientos' )
                                    } )
                            } )
                    } )
            }
        } );

        return response.json( {data: cliente} );
    }

    async save( {request, response, auth} ) {
        const transaccion = await Database.beginTransaction();

            const user      = await auth.getUser();
            const cliente   = await Cliente.findBy( 'user_id',      user.id );
            const reserva   = await Reserva.create( {
                cliente_id:                         cliente.id,
                presentacion_pelicula_horario_id:   request.input( 'presentacion_pelicula_horario_id' ),
                fecha:                              new Date(),
                asientos_reservados:                request.input( 'asientos' ).lenght,
            }, transaccion );

            let asientos = [];
            for (let i = 0; i < request.input( 'asientos' ).length; i++) {
                const asiento_actual    = request.input( 'asientos' )[i];
                const asiento_fila      = asiento_actual.split( '-' );
                asientos.push( {
                    reserva_id: reserva.id,
                    fila:       asiento_fila[0],
                    numero:     asiento_fila[1],
                    estado:     'RESERVADO'
                } );
            }

            await Asiento.createMany( asientos, transaccion );

        transaccion.commit();

        return response.json( {data: {respuesta: 'OK'} } );
    }

}

module.exports = ReservaController
