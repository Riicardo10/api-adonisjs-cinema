'use strict'


/** @type {import('@adonisjs/lucid/src/Factory')} */

const Factory = use('Factory');
const Hash = use('Hash');

Factory.blueprint('App/Models/User', async (faker) => {
    return {
        username:   faker.username(),
        email:      faker.email(),
        password:   await Hash.make(faker.password()),
        created_at: faker.date()
    }
});

Factory.blueprint('App/Models/Cliente', async (faker) => {
    return {
        user_id: async () => {
            return (await Factory.model('App/Models/User').create()).id
          },
          telefono:         faker.phone(),
        tarjeta_credito:    faker.cc( {type: 'Mastercard'} )
    }
});

Factory.blueprint('App/Models/Cinema', async (faker) => {
    const nombre = faker.company();
    return {
        nombre,
        screenshot: 'http://fakeimg.pl/600x400/?text=' + nombre,
        direccion:  faker.address(),
        telefono:   faker.phone(),
        asientos:   faker.integer( {min:300, max:1000} ),
        detalles:   faker.sentence( {words:30} )
    }
});

Factory.blueprint('App/Models/Sala', async (faker, i, data) => {
    const filas = faker.integer( {min:5, max:20} );
    return {
        cinema_id:  data.cinema_id,
        filas,
        asientos:   filas * 10,
        numero:     faker.integer( {min:1, max:20} )
    }
});

Factory.blueprint('App/Models/Pelicula', async (faker) => {
    const nombre = faker.sentence( {words: 3} );
    return {
        nombre,
        director:   faker.name(),
        screenshot: 'http://fakeimg.pl/600x400/?text=' + nombre,
        sinopsis:   faker.sentence( {words: 50} ),
        created_at: faker.date()
    }
});

Factory.blueprint('App/Models/PresentacionPelicula', async (faker, i, data) => {
    return {
        cinema_id:      data.cinema_id,
        pelicula_id:    data.pelicula_id,
        sala_id:        data.sala_id,
        fecha:          new Date()
    }
});

Factory.blueprint('App/Models/PresentacionPeliculaHorario', async (faker, i, data) => {
    return {
        presentacion_pelicula_id:   data.presentacion_pelicula_id,
        horario:                    faker.hour() + ':00',
    }
});

Factory.blueprint('App/Models/Genero', async (faker, i, data) => {
    return {
        nombre: data.nombre,
    }
});

Factory.blueprint('App/Models/Reserva', async (faker, i, data) => {
    return {
        cliente_id:                         data.cliente_id,
        presentacion_pelicula_horario_id:   data.presentacion_pelicula_horario_id,
        fecha:                              new Date(),
        asientos_reservados:                1
    }
});

Factory.blueprint('App/Models/Asiento', async (faker, i, data) => {
    return {
        fila:   faker.integer( {min:1, max:10} ),
        numero: faker.integer( {min:1, max:100} ),
        estado: 'RESERVADO',
        id:     data.reserva_id
    }
});

