'use strict'

const Factory = use('Factory')

class DatabaseSeeder {
    async run () {

        const clientes = await Factory
            .model( 'App/Models/Cliente' )
            .createMany( 5 );

        const cinemas = await Factory
            .model( 'App/Models/Cinema' )
            .createMany( 10 );

        const generoAccion = await Factory
            .model( 'App/Models/Genero' )
            .create( {nombre: 'Accion'} );
            
        const generoComedia = await Factory
            .model( 'App/Models/Genero' )
            .create( {nombre: 'Comedia'} );

        cinemas.forEach( async cinema => {
            for (let i = 1; i <= 5; i++) {
                const sala = await Factory
                    .model( 'App/Models/Sala' )
                    .create( {cinema_id: cinema.id} );
                const pelicula = await Factory
                    .model( 'App/Models/Pelicula' )
                    .create();
                await pelicula.generos().attach( [generoAccion.id, generoComedia.id] );
                const presentacion_pelicula = await Factory
                    .model( 'App/Models/PresentacionPelicula' )
                    .create( {
                        cinema_id: cinema.id,
                        sala_id: sala.id,
                        pelicula_id: pelicula.id
                    } );
                const presentacion_pelicula_horario = await Factory
                    .model( 'App/Models/PresentacionPeliculaHorario' )
                    .create( {
                        presentacion_pelicula_id: presentacion_pelicula.id
                    } );
                clientes.forEach( async (cliente) => {
                    const reserva = await Factory
                        .model( 'App/Models/Reserva' )
                        .create( {
                            cliente_id: cliente.id,
                            presentacion_pelicula_horario_id: presentacion_pelicula_horario.id
                        } );
                    await Factory
                        .model( 'App/Models/Asiento' )
                        .create( {
                            reserva_id: reserva.id
                        } );
                } );
            }
        });


    }
}

module.exports = DatabaseSeeder
