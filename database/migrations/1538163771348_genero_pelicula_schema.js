'use strict'

const Schema = use('Schema')

class GeneroPeliculaSchema extends Schema {
  up () {
    this.create('genero_pelicula', (table) => {
      table.integer('pelicula_id').unsigned();
      table.foreign('pelicula_id').references('peliculas.id');
      table.integer('genero_id').unsigned();
      table.foreign('genero_id').references('generos.id');
    })
  }

  down () {
    this.drop('genero_pelicula')
  }
}

module.exports = GeneroPeliculaSchema
