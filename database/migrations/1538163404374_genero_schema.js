'use strict'

const Schema = use('Schema')

class GeneroSchema extends Schema {
  up () {
    this.create('generos', (table) => {
      table.increments();
      table.string('nombre', 100);
    })
  }

  down () {
    this.drop('generos')
  }
}

module.exports = GeneroSchema
