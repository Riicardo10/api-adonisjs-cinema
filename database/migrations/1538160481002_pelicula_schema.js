'use strict'

const Schema = use('Schema')

class PeliculaSchema extends Schema {
  up () {
    this.create('peliculas', (table) => {
      table.increments()
      table.string('nombre', 120);
      table.string('director', 100);
      table.string('screenshot', 120).nullable();
      table.text('sinopsis');
      table.timestamps();
    })
  }

  down () {
    this.drop('peliculas')
  }
}

module.exports = PeliculaSchema
