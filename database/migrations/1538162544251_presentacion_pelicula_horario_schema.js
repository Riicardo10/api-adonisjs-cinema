'use strict'

const Schema = use('Schema')

class PresentacionPeliculaHorarioSchema extends Schema {
  up () {
    this.create('presentacion_pelicula_horarios', (table) => {
      table.increments()
      table.integer('presentacion_pelicula_id').unsigned();
      table.foreign('presentacion_pelicula_id').references('presentacion_peliculas.id');
      table.string('horario', 20);
    })
  }

  down () {
    this.drop('presentacion_pelicula_horarios')
  }
}

module.exports = PresentacionPeliculaHorarioSchema
