'use strict'

const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
    this.create('clientes', (table) => {
      table.increments()
      table.integer('user_id').unsigned();
      table.foreign('user_id').references('users.id');
      table.string('telefono', 15).nullable();
      table.string('tarjeta_credito', 40);
      table.timestamps()
    })
  }

  down () {
    this.drop('clientes')
  }
}

module.exports = ClienteSchema
