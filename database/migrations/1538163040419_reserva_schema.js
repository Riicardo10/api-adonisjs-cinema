'use strict'

const Schema = use('Schema')

class ReservaSchema extends Schema {
  up () {
    this.create('reservas', (table) => {
      table.increments();
      table.integer('cliente_id').unsigned();
      table.foreign('cliente_id').references('clientes.id');
      table.integer('presentacion_pelicula_horario_id').unsigned().comment('Pelicula que verán');
      table.foreign('presentacion_pelicula_horario_id').references('presentacion_pelicula_horarios.id');
      table.dateTime('fecha').comment('Fecha/Hora de la reserva');
      table.integer('asientos_reservados').comment('Cantidad de asientos reservados');
    })
  }

  down () {
    this.drop('reservas')
  }
}

module.exports = ReservaSchema
