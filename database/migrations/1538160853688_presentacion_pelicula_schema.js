'use strict'

const Schema = use('Schema')

class PresentacionPeliculaSchema extends Schema {
  up () {
    this.create('presentacion_peliculas', (table) => {
      table.increments();
      table.integer('cinema_id').unsigned();
      table.foreign('cinema_id').references('cinemas.id');
      table.integer('pelicula_id').unsigned();
      table.foreign('pelicula_id').references('peliculas.id');
      table.integer('sala_id').unsigned();
      table.foreign('sala_id').references('salas.id');
      table.date('fecha');
      table.timestamps()
    })
  }

  down () {
    this.drop('presentacion_peliculas')
  }
}

module.exports = PresentacionPeliculaSchema
