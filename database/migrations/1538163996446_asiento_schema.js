'use strict'

const Schema = use('Schema')

class AsientoSchema extends Schema {
  up () {
    this.create('asientos', (table) => {
      table.increments()
      table.integer('fila').comment('Fila de la sala');
      table.integer('numero').comment('Numero de asiento');
      table.enum('estado', ['DISPONIBLE','RESERVADO']).default('DISPONIBLE');
      table.integer('reserva_id').nullable().unsigned();
      table.foreign('reserva_id').references('reservas.id');
    })
  }

  down () {
    this.drop('asientos')
  }
}

module.exports = AsientoSchema
