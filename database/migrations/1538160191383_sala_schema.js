'use strict'

const Schema = use('Schema')

class SalaSchema extends Schema {
  up () {
    this.create('salas', (table) => {
      table.increments()
      table.integer('cinema_id').unsigned();
      table.foreign('cinema_id').references('cinemas.id');
      table.integer('filas');
      table.integer('asientos');
      table.integer('numero');
    })
  }

  down () {
    this.drop('salas')
  }
}

module.exports = SalaSchema
