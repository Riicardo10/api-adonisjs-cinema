'use strict'

const Schema = use('Schema')

class CinemaSchema extends Schema {
  up () {
    this.create('cinemas', (table) => {
      table.increments()
      table.string('nombre', 100);
      table.string('screenshot', 120);
      table.string('direccion', 100);
      table.string('telefono', 15);
      table.integer('asientos');
      table.text('detalles');
    })
  }

  down () {
    this.drop('cinemas')
  }
}

module.exports = CinemaSchema
